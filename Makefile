DIRS = ./main ./include ./config ./doc ./www \
	   ./modules/configparser \
	   ./modules/listenerhandler ./modules/httphandler
CFLAG = -Wall -D_GNU_SOURCE
OBJS = pdsshttp.o 
INCL = -I ./include
LIBCONFIGPAR = ./modules/configparser/configparser.a
LIBHANDER = ./modules/listenerhandler/listenerhandler.a
LIBPARSER = ./modules/httpparser/http_parser.c ./modules/httphandler/httphandler.cpp

all: pdsshttp pdsshttp_kill

pdsshttp: $(OBJS)
	cd ./modules/listenerhandler ; make
	cd ./modules/configparser ; make 
	g++ -o $@ $(OBJS)  $(LIBPARSER) $(LIBCONFIGPAR) $(LIBHANDER) -lpthread 
pdsshttp.o: ./main/pdsshttp.cpp
	g++ -c ./main/pdsshttp.cpp $(INCL)

pdsshttp_kill: ./main/pdsshttp_kill.cpp
	g++ -o pdsshttp_kill ./main/pdsshttp_kill.cpp $(INCL)

.PHONY: clean
clean:
	rm -f *.o *~ pdsshttp core pdsshttp_kill
	for i in $(DIRS); do \
		(cd $$i && make clean) || exit 1; \
	done
