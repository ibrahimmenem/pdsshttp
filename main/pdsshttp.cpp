/* TODO TBD Copyrights and licensing:
 *
 */

/* Description: 
 * Simple implementation of low level HTTP server 
 */

/* Style: 
 * 1. Use tabs instead of spaces (recommended size 4) 
 * 2. Stay within 80 columns width
 * 3. Avoid or minimize the use of single line comments "//" 
 * 4. Use name space in cpp files not in header files  
 * TODO
 */

/* Includes  */

#include "pdsshttp.h"


using namespace std;

/* Global variables  */

string Helpstring = "***Bad arguments***\n\nUsage:\n \
\t./pdsshttp configfile.cfg";
map <string,string> config;
/*initialize mimetypes static member (map)
must be declared in class but defined outside class and main */
HttpHandler::MimeType HttpHandler::mimeTypes_(HttpHandler::fillMimetype());


int shmid;
key_t key;


void* checkfinish (void* unused)
{
char *shm ;
/* Attach the shared memory segment */    
if ((shm = (char *)shmat(shmid, NULL, 0)) == (char *) -1) {
    perror("shmat");
    exit(1);
}
/*
 * wait until the other process changes the 
 * first character of the shared memory to 't'
 */
*shm=0;
while (*shm != 't'){
    //fprintf(stderr, "check shmem for termination signal\n");
    sleep(1);
}
fprintf(stderr, "Kill signal received\nEXIT\n");
exit(0);
}



int main(int argc, char* argv[])
{
/*Check input*/
if (argc !=2)
{
	cout <<  Helpstring << endl;
	exit (EXIT_FAILURE);
}
const char* fileName= argv[1];



/* Instantiate parser object and parse configuration file */

ConfigParser C(fileName); 
config=C.parse();
/* Print loaded configurations*/

map<string,string>::iterator i;
cout<<"Loaded configuration:"<< endl;
for(i=config.begin(); i != config.end(); ++i) 
	cout << i->first <<":"<<setw(30-(i->first.length()))<<i->second<< " "<<endl;
cout << endl;

/* 
  Create a shared memory and launch a thread to periodically check shared 
  memory for exit signal
 */

key = 5678;
/* Create the shared memory segment*/
if ((shmid = shmget(key, 1, IPC_CREAT | 0666)) < 0) {
    perror("shmget");
    exit(1);
}
 
pthread_t thread_id;
pthread_create(&thread_id, NULL, &checkfinish, NULL);

/* Start server */
ListenerHandler ls(config);
ls.empezar();

return 0;

}



