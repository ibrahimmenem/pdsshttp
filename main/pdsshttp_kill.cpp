/*
 * client program to kill pdsshttp using shared memory
 */
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h> 
 
main()
{
    int shmid;
    key_t key;
    char *shm, *s;
    key = 5678;

    if ((shmid = shmget(key, 1, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    if ((shm = (char *)shmat(shmid, NULL, 0)) == (char *) -1) {
        perror("shmat");
        exit(1);
    }

    *shm = 't';

    exit(0);
}
