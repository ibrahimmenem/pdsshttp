#include "../../include/httphandler.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <string.h>

using namespace std;

char empty_string[] = "";

extern  std::map <std::string,std::string> config;
 
char*  getextension(char* filename){
    char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return empty_string;
    return dot + 1;
}	

int on_url(http_parser* parser, const char* at, size_t length) {
	(void)parser;
	char *url=(char *)malloc(sizeof(char)*500);

	sprintf(url, "%.*s\n", (int)length, at);

//	parser->url=(char*)url;
   
	parser->filename=(char*)basename(  url);
	parser->dirname=(char*)dirname(  url);
	parser->data=url;
	return 0;
}

int on_header_field(http_parser* parser, const char* at, size_t length) {
	(void)parser;
	//printf("Header field: %.*s\n", (int)length, at);
 	return 0;
}

int on_header_value(http_parser* _, const char* at, size_t length) {
	(void)_;
	//printf("Header value: %.*s\n", (int)length, at);
	return 0;
}


HttpHandler::MimeType HttpHandler::fillMimetype()
{
    MimeType ret;
	ret["html"]	="text/html";
	ret["htm"]	="text/html";
	ret["jpeg"]	="image/jpeg";
	ret["jpg"]	="image/jpg";
	ret["gif"]	="image/gif";
	ret["png"]	="image/png";
    return ret;
}



HttpHandler::HttpHandler(int fd){

	fd_=fd;
	config_=config;


} 

int 
HttpHandler::handle(){
	char buffer[512];
	int readbytes,i;
	int indexfd, len,ret;
	char  path[] =""  ;
	printf("Receiving\n");

	bzero(buffer,512);
	readbytes= read(fd_,buffer,512);
	if (readbytes < 0) {
		fprintf(stderr,"ERROR reading from socket\n");
		exit(1);
	}

	http_parser_settings settings;
	memset(&settings, 0, sizeof(settings));
	settings.on_url = on_url;
	settings.on_header_field = on_header_field;
	settings.on_header_value = on_header_value;

	/* Create parser struct*/
	http_parser parser;
	http_parser_init(&parser, HTTP_REQUEST);
	size_t nparsed = http_parser_execute(&parser, &settings, buffer  ,\
			strlen(buffer ));
	/*
	printf(" main dirname %s\n", parser.dirname);
	printf(" main filename %s\n", parser.filename);
	*/

	/* response */
	strcpy(path,"."); 
	strcat(path,parser.dirname );

	strcat(path,"/");
	strcat(path,parser.filename);
	path[strlen(path)-1] = 0; 
    if (!strcmp (path,"./")  ){
		fprintf(stderr, "Redirect to default file index.html\n");
		sprintf(buffer,"HTTP/1.1 302 Found\r\nLocation:/www/index.html\r\n\r\n");
		write(fd_,buffer,strlen(buffer));
	}
	else{
	//path=strcat(const_cast<char *>(path),parser.url);

	fprintf( stderr,"Requested file:  %s\n",  path);

	indexfd = open( (const char*) path  , O_RDONLY  );
	if (indexfd<0) {
		printf("Error opening file: %s\n",parser.filename);
		indexfd = open( "./www/error.html" , O_RDONLY  );
		len = (long)lseek(indexfd, (off_t)0, SEEK_END);
		    lseek(indexfd, (off_t)0, SEEK_SET);

		sprintf(buffer,"HTTP/1.1 404 !\r\nServer: pdsshttp/0.0\n"
		"Content-Length: %d\r\nConnection: close\r\nContent-Type:text/html"
        "\r\n\r\n",len);	
		
	}
	else{
	char * extension=getextension(parser.filename);
	//printf( "extension :  \"%s\"\n",  extension);	
	len = (long)lseek(indexfd, (off_t)0, SEEK_END);
		lseek(indexfd, (off_t)0, SEEK_SET);
	extension[strlen(extension)-1]=0;
	sprintf(buffer,"HTTP/1.1 200 OK\r\nServer: pdsshttp/0.0\r\n"
		"Content-Length: %d\r\nConnection: close\r\nContent-Type: %s\r\n\r\n",len,
		 (mimeTypes_[extension]).c_str());
	//printf( "HEADER :  %s\n",  buffer);
	}
	
	
	
    /*Send header*/ 
	write(fd_,buffer,strlen(buffer));


    /*Send requested file*/
	while (	(ret = read(indexfd, buffer, 511)) > 0 ) {
		write(fd_,buffer,ret);
	}

	if (ret < 0){
		fprintf(stderr,"ERROR writing to  socket\n");
		exit(1);
	}
	}
    free(parser.data);//free allocated buffer in on on_url
	close(fd_);

return 0;
} 


 


 

 
