#include "configparser.h"
#include "tinystr.h"
#include "tinyxml.h"
#include <iostream>
#include <string>

using namespace std;


ConfigParser::ConfigParser(const char* fileName) 
{
	cfgFileName_ = fileName ;
}


map<string,string> ConfigParser::parse(void) 
{

    cout<< "\n==================== Loading ... ================= " <<endl;

	map<string,string> configMap;

    /*
     * user tinyXML, obtener information from configure.xml
     *server.ip
     *server.port
     *server.listen_num
     *...
     */

    const char* pfilename = cfgFileName_;
    TiXmlDocument doc(pfilename);
    bool load_okay = doc.LoadFile();
    if(load_okay)
    {
        cout<< "load configure file: " << pfilename << " OK!" << endl;
    }
    else
    {
        cout<< " load configure file: "<< pfilename << " fail!" << endl;
        cout<< " ~~~~~~~~~~~~~~~ error! ~~~~~~~~~~~~~~~"<< endl;
        exit(1);
    }


    /* get root element */
    TiXmlElement * root_element = doc.RootElement();
    if( root_element == NULL) 
    {
        cout<< " get root element fail! " << endl;
        cout<< " ~~~~~~~~~~~~~~~ error! ~~~~~~~~~~~~~~~"<< endl;
        exit(1);
    }
    //cout<< root_element->Value() << endl;

    /* get item node and  value */
    
    for(TiXmlNode* item = root_element->FirstChild();
            item;
            item = item->NextSibling("item") )
    {


        for(TiXmlNode* child = item->FirstChild();
                child; 
                child = item->IterateChildren(child))
        {
            const char * name = child->ToElement()->Value();
            const char * server = child->ToElement()->GetText();
            cout << name << " ------ " << server <<endl;
            configMap[name] = server;
        }
        
    }

    cout<< "==================== Done ... ==================== \n" <<endl;
	return configMap;
}


const char*
ConfigParser::get_cfgFileName(){
	return cfgFileName_;
}

