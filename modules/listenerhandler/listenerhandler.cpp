#include "listenerhandler.h"

using namespace std;

pthread_mutex_t mymutex;
int numThread;
void errors(const char *msg)
{
   // cerr << msg;
    exit(1);
}
/*
This method create the object handler to
serve the web that is request
*/
void* createHandler(void *arg){

	long long sig1 = reinterpret_cast<long long> (arg);
	int newsockfd = static_cast<int>(sig1);
	if (newsockfd < 0) 
		 errors("ERROR on accept");
	HttpHandler HL(newsockfd);
	HL.handle();
	pthread_mutex_lock(&mymutex);
	numThread--;
	pthread_mutex_unlock(&mymutex);	

}
void createThread(int fd)
{

  	pthread_t t1;
	pthread_create(&t1, NULL, &createHandler,((void *)fd));
	void* result;
	pthread_mutex_unlock(&mymutex);
	//pthread_join(t1,&result);

}

void crearSocket(int portno,int nthreads){
	
	/* variables */
	int sockfd, newsockfd;
    socklen_t clilen;

    int n;
    struct sockaddr_in serv_addr, cli_addr;
	pthread_mutex_init(&mymutex, NULL);
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        errors("ERROR opening socket");
	
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
int optval=1;
setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              errors("ERROR on binding");
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
   	numThread = 0;
	while(1){
	bzero((char *) &serv_addr, sizeof(serv_addr));
	
   		newsockfd = accept(sockfd, 
                 	(struct sockaddr *) &cli_addr, 
                 	&clilen);
	pthread_mutex_lock(&mymutex);
	if (numThread < nthreads){
		numThread ++ ;
		printf("number of active connections:\t%d\n",numThread);
		createThread(newsockfd);
	}else{
		pthread_mutex_unlock(&mymutex);
		n = write(newsockfd,"Server is busy please try later",31);
		close(newsockfd);
		printf("****Connection refused***\t\n");
		if (n < 0) errors("ERROR writing to socket");
	}
	}
	
	//close(sockfd);

}
ListenerHandler::ListenerHandler(map <string,string> config) 
{
	nport = atoi(config["server.port"].c_str()); 
	nthread = atoi(config["server.listen_num"].c_str());
}

void ListenerHandler::empezar(){
	printf("Listening on port: %d \n ",nport);
	crearSocket(nport,nthread);
}









