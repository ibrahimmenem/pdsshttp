#!/usr/bin/env python

import thread
import time
import os ,sys


# Define a function for the thread
def wget(  delay):
    time.sleep(delay)
    os.system("wget 127.0.0.1:5050/www/"+str(sys.argv[2]))

# Create two threads as follows
try:
    for i in xrange(int(sys.argv[1])):
        thread.start_new_thread( wget, (5, ) )
except:
   print "Error: unable to start thread"

while 1:
   pass
