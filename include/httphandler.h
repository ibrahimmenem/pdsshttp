#ifndef HTTPHANDLER_H 
#define HTTPHANDLER_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <libgen.h>
#include "http_parser.h"
#include <map>
#include <string>
#include <unistd.h> /* read and write*/
#include <iostream>
#include <fstream>

class HttpHandler {

public:

	HttpHandler(int fd ); 
	int handle();

private:
	std::map<std::string,std::string> config_;
	int fd_;
	/* Static member will be defined only once, for all instances of the class*/  
	typedef std::map<std::string, std::string> MimeType;
    static MimeType fillMimetype();
    static MimeType mimeTypes_;
	

};

#endif
