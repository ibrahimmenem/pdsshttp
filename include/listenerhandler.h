#ifndef LISTENERHANDLER_H 
#define LISTENERHANDLER_H

#include <pthread.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include "httphandler.h"


class ListenerHandler {

private:
	int nport;
	int nthread;

public:
	ListenerHandler(std::map <std::string,std::string> );
	void empezar();
	
};

#endif
