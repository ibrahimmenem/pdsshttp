#ifndef CONFIGPARSER_H 
#define CONFIGPARSER_H


#include <map>
#include <string>


class ConfigParser {

private:
   const char*  cfgFileName_;

public:

	ConfigParser(const char* fileName);
	std::map<std::string,std::string> parse(void);
	const char* get_cfgFileName();
   
};

#endif
